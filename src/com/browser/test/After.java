package com.browser.test;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.sikuli.script.Screen;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.RunTime;

public class After {
	WebDriver driver;
	
	/**
	 * applaunch
	 * @throws InterruptedException
	 */
	public void applaunch() throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "./Driver/chromedriver.exe");
		driver = new ChromeDriver ();
		
		driver.manage().window().maximize();
		
		Thread.sleep(6000);
		
		driver.get("https://demo.guru99.com/test/newtours/");
		}
	
	
	/**
	 * login with username & password
	 * submit
	 * use "Xpath" identifier
	 * @throws InterruptedException
	 */
	public void loginMercury() throws InterruptedException {
		
		Thread.sleep(6000);
		WebElement uName = driver.findElement(By.name("userName"));
        uName.sendKeys("sudeshna");
		
		WebElement password = driver.findElement(By.name("password"));
		password.sendKeys("S@123");
		
		WebElement submit = driver.findElement(By.name("submit"));
		//submit.click();
		
		WebElement register = driver.findElement(By.xpath("//a[@href='register.php']"));
		//register.click();
		
		//driver.close();
		
	}
	
	/**
	 * for creat permanent
	 * @param username
	 * @param password
	 */
	public void loginWithParameter(String username, String password) {
		 
		WebElement uName = driver.findElement(By.name("userName"));
		uName.sendKeys(username);
		WebElement password1 = driver.findElement(By.name("password"));
		password1.sendKeys(password);
		WebElement submit = driver.findElement(By.name("submit"));
		submit.click();
	}
	
	
	/**
	 * click on "flight"
	 * select dropdown with threemethod
	 */
	public void departFromSelect() {
		
		WebElement flightlink = driver.findElement(By.linkText("Flights"));
		flightlink.click();
		WebElement departDropDown = driver.findElement(By.name("fromPort"));
		Select sl = new Select(departDropDown);
		//sl.selectByVisibleText("Paris");
		//sl.selectByValue("New York");
		//sl.selectByIndex(2);
		
		WebElement onDropDownMonth = driver.findElement(By.name("fromMonth"));
		Select sl1 = new Select(onDropDownMonth);
		sl1.selectByVisibleText("March");
		
		WebElement onDropDownDate = driver.findElement(By.name("fromDay"));
		Select sl2 = new Select(onDropDownDate);
		sl2.selectByValue("14");
		
		WebElement arrivingLink = driver.findElement(By.name("toPort"));
		Select sl3 = new Select(arrivingLink);
		sl3.selectByIndex(4);
		
		WebElement returnlinkMonth = driver.findElement(By.name("toMonth"));
		Select sl4 = new Select(returnlinkMonth);
		sl4.selectByVisibleText("May");
		
		WebElement returnlinkDate = driver.findElement(By.name("toDay"));
		Select sl5 = new Select(returnlinkDate);
		sl5.selectByIndex(3);
		
	}
	
	/**
	 * for controlling mouse
	 */
	public void loginAction()  {
		
		WebElement userName = driver.findElement(By.xpath("//input[@type='text']"));
		userName.sendKeys("saha");
		WebElement pswrd = driver.findElement(By.xpath("//input[@type='password']"));
		pswrd.sendKeys("1234");
		WebElement submit = driver.findElement(By.name("submit"));
		
		Actions builder = new Actions(driver);
		builder.moveToElement(submit).build().perform();
		builder.click(submit).build().perform();
		
	}
	
	/**
	 * for control keyboard
	 * @throws AWTException
	 */
	public void loginRobot() throws AWTException {
		
		WebElement userName = driver.findElement(By.xpath("//input[@type='text']"));
		userName.click();
		
		Robot r1 = new Robot();
		
		//r1.keyPress(KeyEvent.VK_CAPS_LOCK);
		r1.keyPress(KeyEvent.VK_S);
		//r1.keyRelease(KeyEvent.VK_CAPS_LOCK);
		r1.keyPress(KeyEvent.VK_A);
		r1.keyPress(KeyEvent.VK_H);
		r1.keyPress(KeyEvent.VK_A);
		
		
		r1.keyPress(KeyEvent.VK_TAB);

		
		r1.keyPress(KeyEvent.VK_1);
		r1.keyPress(KeyEvent.VK_2);
		//r1.keyPress(KeyEvent.VK_ENTER);
		
	}

	/**
	 * login with property file data
	 * @throws IOException 
	 */
	public void loginWithPropertyFile() throws IOException {
		
		File file = new File("./testdata/testdata.properties");
		FileInputStream fileInput = new FileInputStream(file);
		Properties prop = new Properties();
		prop.load(fileInput);
		
		WebElement userName = driver.findElement(By.xpath("//input[@type='text']"));
		userName.sendKeys(prop.getProperty("employeename"));
		WebElement pswrd = driver.findElement(By.xpath("//input[@type='password']"));
		pswrd.sendKeys(prop.getProperty("employeepassword"));
		WebElement submit = driver.findElement(By.name("submit"));
		
	}
	/**
	 * login with Sikuli(by screenshot)
	 * @throws FindFailed 
	 */
	 public void loginSikuli() throws FindFailed {
		 
		 WebElement uName = driver.findElement(By.name("userName"));
	        uName.sendKeys("sudeshna");
			
			WebElement password = driver.findElement(By.name("password"));
			password.sendKeys("S@123");
			
		
		Screen screen = new Screen();
		//Pattern btnSubmit = new Pattern("./sikulifiles/btnsubmit.png");
		//screen.click(btnSubmit);
		
		//Pattern windowClck = new Pattern("./sikulifiles/Capture.PNG");
		//screen.click(windowClck);
		
		}
	 
	 /**
	  * photo upload by autoIt
	  * @throws IOException
	  */
	 public void autoITTest() throws IOException {
		 
		 System.setProperty("webdriver.chrome.driver", "./Driver/chromedriver.exe");
			driver = new ChromeDriver ();
			driver.manage().window().maximize();
			driver.get("https://imgbb.com/");
			
			WebElement startBtnClick = driver.findElement(By.xpath("//a[@class='btn btn-big blue']"));
			startBtnClick.click();
			
			Runtime.getRuntime().exec("./autoITexe/SelectImage.exe");
			
			}
	 /**
	  * 
	  */
	 public void webTableHandling() {
		 
		 System.setProperty("webdriver.chrome.driver", "./Driver/chromedriver.exe");
			driver = new ChromeDriver ();
			driver.manage().window().maximize();
			driver.get("https://demo.guru99.com/test/web-table-element.php");
			
			//No. of columns
			List<WebElement> col = driver.findElements(By.xpath(".//*[@id='leftcontainer']/table/thead/tr/th"));
			
			
			 
	 }
	 /**
	  * 
	  */
	 public void loginFromExcel() {
		
		 File src = new File("./testdata/excel.xls");
		 Workbook wb = Workbook.getWorkbook(src);
		 
		 
	 }
	 
}

	 
	


	
		
